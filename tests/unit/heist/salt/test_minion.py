import json
import pathlib
import tarfile
import tempfile
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch

import asyncssh
import asynctest
import pytest
from dict_tools.data import NamespaceDict

from tests.support.helpers import TEST_FILES


class ConfigTestData:
    def __init__(self):
        self.tname = "12344175aa612d560b0a89917d6b3f7zc3c3c9dcb40b8319a3335e0f0463210a"
        self.remote = NamespaceDict(
            host="192.168.1.2",
            username="root",
            password="test_password",
            id="testarget_name",
            tunnel="asyncssh",
        )
        self.roster = {
            self.tname: {
                "host": "192.168.1.59",
                "username": "root",
                "password": "testpasswd",
                "master_port": 1234,
                "minion_opts": {"master_port": 5678},
                "id": "test_id",
                "tunnel": "asyncssh",
            }
        }


@pytest.fixture()
def test_data():
    return ConfigTestData()


@pytest.mark.asyncio
async def test_manage_tunnel(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and no tunnel created
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.minion.manage_tunnel = hub.heist.salt.minion.manage_tunnel
    assert await mock_hub.heist.salt.minion.manage_tunnel(
        test_data.tname, "asyncssh", remote=test_data.remote
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    mock_hub.tunnel.asyncssh.tunnel.assert_not_called()


@pytest.mark.asyncio
async def test_manage_tunnel_with_salt_tunnel(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and the salt tunnel created
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.minion.manage_tunnel = hub.heist.salt.minion.manage_tunnel
    config = mock_hub.tool.config.get_minion_opts(
        run_dir=pathlib.Path("root"),
        target_name="1234",
        target_os="linux",
        minion_id="test_minion",
    )
    assert await mock_hub.heist.salt.minion.manage_tunnel(
        test_data.tname,
        "asyncssh",
        remote=test_data.remote,
        tunnel=True,
        minion_opts=config,
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    assert mock_hub.tunnel.asyncssh.tunnel.call_count == 2


@pytest.mark.parametrize("py_version", [(3, 6), (3, 7)])
@pytest.mark.asyncio
async def test_run(hub, mock_hub, py_version):
    mock_hub.heist.salt.minion.run = hub.heist.salt.minion.run
    remotes = {"test_host": {"host": "192.168.1.2"}}
    mock_gather = asynctest.CoroutineMock()
    patch_gather = asynctest.mock.patch("asyncio.gather", mock_gather)
    patch_version = asynctest.patch("sys.version_info", Mock(return_value=py_version))
    with patch_gather, patch_version:
        await mock_hub.heist.salt.minion.run(remotes=remotes)
    if py_version == (3, 6):
        mock_gather.call_args_list[0][1] == {"return_exceptions": False}
        mock_gather.call_args_list[0][1] == {"loop": hub.pop.Loop}
        len(mock_gather.call_args_list[0]) == 3
    else:
        mock_gather.call_args_list[0][1] == {"return_exceptions": False}
        len(mock_gather.call_args_list[0]) == 2


@pytest.mark.asyncio
async def test_manage_tunnel_with_salt_tunnel_bootstrap(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and the tunnel is true
    but bootstrap is True. It should not create the tunnel.
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.minion.manage_tunnel = hub.heist.salt.minion.manage_tunnel
    config = mock_hub.tool.config.get_minion_opts(
        run_dir=pathlib.Path("root"),
        target_name="1234",
        target_os="linux",
        minion_id="test_minion",
        bootstrap=True,
    )
    assert await mock_hub.heist.salt.minion.manage_tunnel(
        test_data.tname,
        "asyncssh",
        remote=test_data.remote,
        tunnel=True,
        minion_opts=config,
        bootstrap=True,
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    assert mock_hub.tunnel.asyncssh.tunnel.call_count == 0


@pytest.mark.asyncio
async def test_single_offline_mode(hub, mock_hub, test_data):
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.heist.salt.minion.single = hub.heist.salt.minion.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.artifact.salt.deploy.return_value = False
    await mock_hub.heist.salt.minion.single(remote=test_data.remote)
    mock_hub.artifact.salt.repo_data.assert_not_called()
    mock_hub.artifact.init.get.assert_not_called()
    mock_hub.artifact.salt.latest.assert_called_once()


@pytest.mark.asyncio
async def test_single_host_unreachable(hub, mock_hub, test_data):
    """
    test call to single when we cannot connect to target host
    """
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.heist.salt.minion.single = hub.heist.salt.minion.single
    mock_hub.heist.salt.minion.manage_tunnel = hub.heist.salt.minion.manage_tunnel
    mock_hub.tunnel.asyncssh.create.return_value = False
    ret = await mock_hub.heist.salt.minion.single(remote=test_data.remote)
    minion_id = test_data.remote["id"]
    assert ret == {
        "comment": f"Could not establish tunnel with {minion_id}",
        "result": "Error",
        "retvalue": 1,
        "target": minion_id,
    }
    mock_hub.tool.system.os_arch.assert_not_called()


@pytest.mark.asyncio
async def test_single_host_artifact_not_downloaded(hub, mock_hub, test_data):
    """
    test call to single when we cannot download an artifact
    """
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.OPT.heist.offline_mode = False
    mock_hub.heist.salt.minion.single = hub.heist.salt.minion.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.artifact.salt.deploy.return_value = False
    mock_hub.artifact.salt.repo_data.return_value = {
        "latest": {
            "salt-3004.2-1-linux-amd64.tar.gz": {
                "name": "salt-3004.2-1-linux-amd64.tar.gz",
                "version": "3004.2-1",
                "os": "linux",
            }
        }
    }
    mock_hub.artifact.init.get.return_value = False
    mock_hub.tool.artifacts.get_artifact_dir.return_value = pathlib.Path()
    ret = await mock_hub.heist.salt.minion.single(remote=test_data.remote)
    minion_id = test_data.remote["id"]
    assert ret == {
        "result": "Error",
        "comment": "Could not download the artifact",
        "retvalue": 1,
        "target": minion_id,
    }


@pytest.mark.asyncio
async def test_single_host_not_clean_path(hub, mock_hub, test_data, tmp_path):
    """
    test call to single when run_dir is not a clean_path
    """
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.OPT.heist.offline_mode = False
    mock_hub.heist.salt.minion.single = hub.heist.salt.minion.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.artifact.salt.deploy.return_value = False
    mock_hub.tool.path.path_convert.return_value = tmp_path
    mock_hub.artifact.salt.repo_data.return_value = {
        "latest": {
            "salt-3004.2-1-linux-amd64.tar.gz": {
                "name": "salt-3004.2-1-linux-amd64.tar.gz",
                "version": "3004.2-1",
                "os": "linux",
            }
        }
    }
    mock_hub.artifact.init.get.return_value = True
    mock_hub.tool.artifacts.get_artifact_dir.return_value = pathlib.Path()
    mock_hub.tool.path.clean_path.return_value = False
    ret = await mock_hub.heist.salt.minion.single(remote=test_data.remote)
    minion_id = test_data.remote["id"]
    assert ret == {
        "result": "Error",
        "comment": f"The run_dir {tmp_path} is not a valid path",
        "retvalue": 1,
        "target": minion_id,
    }


@pytest.mark.asyncio
async def test_single_bootstrap_return(hub, mock_hub, test_data, tmp_path):
    """
    test call to single when bootstrap and returns successful
    """
    test_data.remote.bootstrap = True
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.OPT.heist.offline_mode = True
    mock_hub.heist.salt.minion.single = hub.heist.salt.minion.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.artifact.salt.deploy.return_value = True
    mock_hub.tool.path.path_convert.return_value = tmp_path
    mock_hub.tool.path.clean_path.return_value = True
    ret = await mock_hub.heist.salt.minion.single(remote=test_data.remote)
    minion_id = test_data.remote["id"]
    assert ret == {
        "result": "Success",
        "comment": f"The minion {minion_id} bootstrapped Salt successfully",
        "retvalue": 0,
        "target": minion_id,
    }


@pytest.mark.asyncio
async def test_manage_tunnel_with_salt_tunnel_cant_connect(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and the salt tunnel cannot be
    created
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.minion.manage_tunnel = hub.heist.salt.minion.manage_tunnel
    mock_hub.tunnel.asyncssh.tunnel.side_effect = asyncssh.misc.ChannelListenError
    config = mock_hub.tool.config.get_minion_opts(
        run_dir=pathlib.Path("root"),
        target_name="1234",
        target_os="linux",
        minion_id="test_minion",
    )
    assert not await mock_hub.heist.salt.minion.manage_tunnel(
        test_data.tname,
        "asyncssh",
        remote=test_data.remote,
        tunnel=True,
        minion_opts=config,
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    assert mock_hub.tunnel.asyncssh.tunnel.call_count == 1
