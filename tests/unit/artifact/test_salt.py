import json
import pathlib
import tarfile
import tempfile
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch

import aiohttp
import pytest
from dict_tools.data import NamespaceDict

from tests.support.helpers import TEST_FILES


@pytest.mark.asyncio
async def test_repo_data(hub, mock_hub):
    """
    test repo_data when json returned
    """
    with open(TEST_FILES / "repo.json") as fp:
        repo_data = json.load(fp)
    mock_hub.artifact.salt.repo_data = hub.artifact.salt.repo_data
    mock_hub.artifact.init.fetch.return_value = repo_data
    mock_hub.OPT = Mock()
    salt_repo_url = "https://testrepo.com/"
    assert (
        await mock_hub.artifact.salt.repo_data(salt_repo_url=salt_repo_url) == repo_data
    )
    assert mock_hub.artifact.init.fetch.call_args[0][1] == salt_repo_url + "repo.json"


@pytest.mark.asyncio
async def test_repo_data_none_returned(hub, mock_hub):
    """
    test repo_data when None is returned
    """
    mock_hub.artifact.salt.repo_data = hub.artifact.salt.repo_data
    mock_hub.artifact.init.fetch.return_value = None
    mock_hub.OPT = Mock()
    salt_repo_url = "https://testrepo.com/"
    assert not await mock_hub.artifact.salt.repo_data(salt_repo_url=salt_repo_url)
    assert mock_hub.artifact.init.fetch.call_args[0][1] == salt_repo_url + "repo.json"


@pytest.mark.asyncio
async def test_get(hub, mock_hub):
    """
    test salt.get artifact
    """
    mock_hub.artifact.salt.get = hub.artifact.salt.get
    mock_hub.artifact.salt.latest.return_value = False
    mock_hub.artifact.init.fetch.return_value = True
    mock_hub.artifact.init.verify.return_value = True
    mock_temp_dir = tempfile.TemporaryDirectory()

    # add artifact
    with tempfile.TemporaryDirectory() as repo_tmpdir:
        with tarfile.open(
            pathlib.Path(mock_temp_dir.name, "salt-3003-3-linux-amd64.tar.gz"), "w:gz"
        ) as tf:
            tf.add(TEST_FILES / "repo.json")

        with open(TEST_FILES / "repo.json") as fp:
            repo_data = json.load(fp)

        artifacts_dir = pathlib.Path(repo_tmpdir) / "repo"
        mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts_dir
        artifact = "salt-3003-3-linux-amd64.tar.gz"
        mock_hub.OPT.heist = NamespaceDict(
            artifacts_dir=artifacts_dir,
            salt_repo_url="https://testrepo.com",
            onedir=False,
        )
        async with aiohttp.ClientSession() as session:
            with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
                tmp_path = await mock_hub.artifact.salt.get(
                    "linux",
                    version="3003",
                    repo_data=repo_data,
                    session=session,
                    tmpdirname=pathlib.Path(mock_temp_dir.name),
                )
        assert tmp_path.exists()
        assert pathlib.Path(mock_temp_dir.name) / artifact == tmp_path


@pytest.mark.parametrize(
    "sudo,exp_perms",
    [
        (True, 710),
        (False, 700),
    ],
)
@pytest.mark.asyncio
async def test_deploy(hub, mock_hub, tmpdir, sudo, exp_perms):
    """
    test the correct path is returned when running test_deploy
    """
    mock_hub.artifact.salt.deploy = hub.artifact.salt.deploy
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    mock_hub.OPT.heist = NamespaceDict(onedir=False, artifacts_dir="")
    mock_tempfile = tempfile.TemporaryDirectory()
    run_dir = pathlib.Path("var") / "tmp" / "test_run_dir"
    tunnel_name = "test_tunnel_name"
    test_minion = "test_minion"
    user = "testuser"
    mock_hub.tunnel.asyncssh.CONS = NamespaceDict({tunnel_name: {"sudo": sudo}})

    with tempfile.TemporaryDirectory() as repo_tmpdir:
        binary = pathlib.Path(repo_tmpdir) / "salt-3003-3-linux-amd64.tar.gz"
        with tarfile.open(binary, "w:gz") as tf:
            tf.add(TEST_FILES / "repo.json")

        patch_os = patch("os.remove", Mock(return_value=True))
        patch_tempfile = patch(
            "tempfile.TemporaryDirectory", Mock(return_value=mock_tempfile)
        )
        with patch_os, patch_tempfile:
            await mock_hub.artifact.salt.deploy(
                tunnel_name,
                "asyncssh",
                run_dir,
                binary,
                minion_id=test_minion,
                user=user,
            )

        root_dir = run_dir / "root_dir"
        assert mock_hub.tunnel.asyncssh.cmd.call_args_list == [
            call(
                tunnel_name,
                f"mkdir -m{exp_perms} -p {root_dir.parent.parent} {root_dir.parent} {root_dir} {root_dir / 'conf'}",
                target_os="linux",
            ),
            call("test_tunnel_name", f"chown -R {user}:{user} var/tmp"),
            call(tunnel_name, f"chmod 744 {run_dir / 'salt'}", target_os="linux"),
        ]

        assert mock_hub.tunnel.asyncssh.send.call_args_list[1] == call(
            tunnel_name,
            [binary],
            run_dir,
            preserve=True,
        )

        assert mock_hub.tool.config.get_minion_opts.call_args_list[0] == call(
            run_dir,
            tunnel_name,
            target_os="linux",
            minion_id=test_minion,
            bootstrap=False,
        )


@pytest.mark.parametrize(
    "versions,exp_ver",
    [
        (["3002-1", "3003", "3004", "3005rc1-2", "3005rc1-3"], "3005rc1-3"),
        (["3003", "3005", "3004"], "3005"),
        (["3003.0", "3003.1", "3003.4"], "3003.4"),
        (["3003.0-3", "3003.1-1", "3003.4-2"], "3003.4-2"),
    ],
)
def test_latest(hub, mock_hub, tmp_path, versions, exp_ver):
    """
    test query the latest with different version numbers
    """
    artifacts_dir = tmp_path / "onedir"
    artifacts_dir.mkdir()
    for version in versions:
        with open(artifacts_dir / f"salt-{version}-linux-amd64.tar.gz", "w"):
            pass
    mock_hub.artifact.salt.latest = hub.artifact.salt.latest
    mock_hub.tool.artifacts.get_artifact_dir = hub.tool.artifacts.get_artifact_dir
    mock_hub.OPT.heist = NamespaceDict(artifacts_dir=tmp_path, onedir=False)
    ret = mock_hub.artifact.salt.latest("salt")
    assert ret == str(artifacts_dir / f"salt-{exp_ver}-linux-amd64.tar.gz")
