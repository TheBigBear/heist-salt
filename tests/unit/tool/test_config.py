import pathlib
from unittest.mock import Mock

import pytest
import yaml


class ConfigTestData:
    def __init__(self):
        self.minion_id = "test_minion"
        self.tname = "12344175aa612d560b0a89917d6b3f7zc3c3c9dcb40b8319a3335e0f0463210a"
        self.run_dir = pathlib.Path("root")

    @property
    def roster_content(self):
        return {
            self.tname: {
                "host": "192.168.1.59",
                "username": "root",
                "password": "testpasswd",
                "id": self.minion_id,
                "tunnel": "asyncssh",
            }
        }

    @property
    def expected_content(self):
        return {
            "grains": {"minion_type": "heist"},
            "master": "127.0.0.1",
            "master_port": 44506,
            "publish_port": 44505,
            "root_dir": str(self.run_dir / "root_dir"),
            "id": self.minion_id,
        }


@pytest.fixture()
def conf_data():
    return ConfigTestData()


def test_mk_config_minion_config(hub, mock_hub, conf_data):
    """
    test mk_config function when passing minion configurations
    """
    tname = "12344175aa612d560b0a89917d6b3f7zc3c3c9dcb40b8319a3335e0f0463210a"
    run_dir = pathlib.Path("root")
    mock_hub.heist.CONS = {tname: {"run_dir": run_dir}}
    mock_hub.tool.config.mk_config = hub.tool.config.mk_config
    mock_hub.tool.config.get_minion_opts = hub.tool.config.get_minion_opts
    roster_content = conf_data.roster_content.copy()
    roster_content[conf_data.tname]["minion_opts"] = {"log_level_logfile": "debug"}
    mock_hub.heist.ROSTERS = roster_content
    config = mock_hub.tool.config.get_minion_opts(
        run_dir=conf_data.run_dir,
        target_name=conf_data.tname,
        target_os="linux",
        minion_id=conf_data.minion_id,
    )
    path = mock_hub.tool.config.mk_config(config)

    with open(path) as fp:
        content = yaml.safe_load(fp)

    expected = conf_data.expected_content.copy()
    expected["log_level_logfile"] = "debug"
    assert content == expected


def test_mk_config_minion_config_with_master_port(hub, mock_hub, conf_data):
    """
    test mk_config function when passing minion configurations
    and setting the same config as master_port
    """
    tname = "12344175aa612d560b0a89917d6b3f7zc3c3c9dcb40b8319a3335e0f0463210a"
    run_dir = pathlib.Path("root")
    mock_hub.heist.CONS = {tname: {"run_dir": run_dir}}
    mock_hub.tool.config.mk_config = hub.tool.config.mk_config
    mock_hub.tool.config.get_minion_opts = hub.tool.config.get_minion_opts
    roster_content = conf_data.roster_content.copy()
    roster_content[conf_data.tname]["master_port"] = "1234"
    roster_content[conf_data.tname]["minion_opts"] = {"master_port": 5678}
    mock_hub.heist.ROSTERS = roster_content

    config = mock_hub.tool.config.get_minion_opts(
        run_dir=pathlib.Path("root"),
        target_name=conf_data.tname,
        target_os="linux",
        minion_id=conf_data.minion_id,
    )
    path = mock_hub.tool.config.mk_config(config)

    with open(path) as fp:
        content = yaml.safe_load(fp)

    expected = conf_data.expected_content.copy()
    expected["master_port"] = 5678
    assert content == expected


def test_get_minion_opts_bootstrap(hub, mock_hub, conf_data):
    """
    test get_minion_opts when bootstrap is set
    """
    mock_hub.tool.config.get_minion_opts = hub.tool.config.get_minion_opts
    roster_content = conf_data.roster_content.copy()
    roster_content[conf_data.tname]["minion_opts"] = {"master": "saltmaster"}

    mock_hub.heist.ROSTERS = roster_content
    config = mock_hub.tool.config.get_minion_opts(
        run_dir=conf_data.run_dir,
        target_name=conf_data.tname,
        target_os="linux",
        minion_id=conf_data.minion_id,
        bootstrap=True,
    )
    assert config == {
        "master": roster_content[conf_data.tname]["minion_opts"]["master"],
        "root_dir": str(conf_data.run_dir / "root_dir"),
        "id": conf_data.minion_id,
        "grains": {"minion_type": "heist"},
    }


def test_get_minion_opts_bootstrap_false(hub, mock_hub, conf_data):
    """
    test get_minion_opts when bootstrap is set to False
    """
    mock_hub.tool.config.get_minion_opts = hub.tool.config.get_minion_opts
    roster_content = conf_data.roster_content.copy()

    mock_hub.heist.ROSTERS = roster_content
    config = mock_hub.tool.config.get_minion_opts(
        run_dir=conf_data.run_dir,
        target_name=conf_data.tname,
        target_os="linux",
        minion_id=conf_data.minion_id,
    )
    assert config == {
        "grains": {"minion_type": "heist"},
        "root_dir": str(conf_data.run_dir / "root_dir"),
        "id": conf_data.minion_id,
        "master": "127.0.0.1",
        "master_port": 44506,
        "publish_port": 44505,
    }
