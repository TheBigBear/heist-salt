import pathlib

import pytest
from dict_tools.data import NamespaceDict


def test_get_salt_path(hub, mock_hub):
    """
    Test get_salt_path
    """
    mock_hub.tool.artifacts.get_salt_path = hub.tool.artifacts.get_salt_path
    run_dir = pathlib.Path("rundir")
    assert (
        mock_hub.tool.artifacts.get_salt_path(run_dir, target_os="linux")
        == run_dir / "salt" / "run" / "run"
    )
