import pytest


@pytest.mark.parametrize(
    "service_plugin,exp_ret", [("systemd", "salt-minion"), ("raw", "minion")]
)
def test_get_service_name(hub, mock_hub, service_plugin, exp_ret):
    """
    Test get_service_name
    """
    mock_hub.tool.service.get_service_name = hub.tool.service.get_service_name
    assert mock_hub.tool.service.get_service_name(service_plugin) == exp_ret
