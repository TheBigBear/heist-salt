from unittest import mock

import pytest


@pytest.fixture(scope="function")
def hub(hub):
    for dyne in ("config", "heist", "salt", "artifact", "tool", "tunnel"):
        hub.pop.sub.add(dyne_name=dyne)
    hub.pop.sub.load_subdirs(hub.salt, recurse=True)
    yield hub


@pytest.fixture(scope="function")
def mock_hub(hub):
    m_hub = hub.pop.testing.mock_hub()
    m_hub.OPT = mock.MagicMock()
    m_hub.SUBPARSER = "salt.minion"
    yield m_hub
