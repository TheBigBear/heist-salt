import pathlib
import tempfile
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch

import pytest
from dict_tools.data import NamespaceDict

from tests.support.helpers import TEST_FILES


@pytest.mark.parametrize(
    "sudo",
    [True, False],
)
@pytest.mark.asyncio
async def test_generate_keys(hub, mock_hub, tmp_path, sudo):
    """
    test generate_keys
    """
    mock_hub.salt.key.init.generate_keys = hub.salt.key.init.generate_keys
    test_run_dir = pathlib.Path("test_run_dir")
    mock_hub.tool.artifacts.get_salt_path = hub.tool.artifacts.get_salt_path
    key_dir = test_run_dir / "root_dir" / "etc" / "salt" / "pki" / "minion"
    binary = test_run_dir / "salt" / "run" / "run"
    target_name = "test_target"
    user = "testuser"
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    mock_hub.tunnel.asyncssh.CONS = NamespaceDict({target_name: {"sudo": sudo}})
    mock_hub.OPT = Mock()
    mock_hub.OPT.heist = NamespaceDict(key_plugin="local_master", onedir=False)
    with patch(
        "salt.config.client_config", Mock(return_value={"pki_dir": tmp_path / "pki"})
    ):
        with patch("pathlib.Path.is_file", Mock(return_value=True)):
            assert await mock_hub.salt.key.init.generate_keys(
                target_name=target_name,
                tunnel_plugin="asyncssh",
                run_dir=test_run_dir,
                minion_id="test_minion",
                target_os="linux",
                user=user,
            )
    perms = 700
    if sudo:
        perms = 710
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
        target_name,
        f"mkdir -m{perms} -p {str(key_dir.parent.parent.parent)} {str(key_dir.parent.parent)} {str(key_dir.parent)} {str(key_dir)}",
        target_os="linux",
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[1] == call(
        "test_target", f"chown -R {user}:{user} test_run_dir/root_dir/etc"
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[2] == call(
        target_name,
        f"{binary} key --gen-keys=minion --gen-keys-dir={key_dir}",
        target_os="linux",
    )
