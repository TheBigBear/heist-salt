import json
import pathlib
from unittest.mock import call

import pytest
from dict_tools.data import NamespaceDict


@pytest.mark.asyncio
async def test_grains(hub, mock_hub):
    """
    test getting grains information
    """
    target_name = "test_name"
    grains = {"local": {"arch_type": "x86_64", "os": "linux"}}
    run_dir = pathlib.Path("/run") / "56ea"
    hub.heist.OS_DEFAULTS = {"linux": {"run_dir_root": run_dir.parent}}
    mock_hub.OPT.heist = NamespaceDict({"onedir": False})
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {
            "returncode": 0,
            "stderr": 0,
            "stdout": "WARNING: THIS IS A WARNING: " + str(json.dumps(grains)),
        }
    )
    mock_hub.tool.artifacts.get_salt_path = hub.tool.artifacts.get_salt_path
    mock_hub.salt.call.init.get_grains = hub.salt.call.init.get_grains
    ret = await mock_hub.salt.call.init.get_grains(
        target_name, "asyncssh", run_dir, target_os="linux"
    )
    assert ret == grains["local"]
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list == [
        call(
            target_name,
            f"{str(run_dir)}/salt/run/run call --config-dir {str(run_dir)}/root_dir/conf --local grains.items --out json",
            target_os="linux",
        )
    ]
