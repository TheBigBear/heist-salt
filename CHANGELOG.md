All notable changes to Heist-Salt will be documented in this file.

This changelog follows [keepachangelog](https://keepachangelog.com/en/1.0.0/) format, and is intended for human consumption.

# Changelog

v5.3.1 (2022-07-13)
===================

Fixed
-----

- Add packaging as a base requirement for heist-salt. (#50)


v5.3.0 (2022-07-12)
===================

Deprecated
----------
- The support for singlebin packages will be removed in v6.0.0. Please use `--onedir` going forward.
  In the v6.0.0 release, onedir will be the default option and the `--onedir` argument will be removed.

Fixed
-----

- Fix permission denied error when using sudo. (#40)

Added
-----

- Allow users to use the new Heist raw service. (#37)
- Add Support for win_service Plugin for Windows (#38)
- Add Windows Support for SingleBin (#39)
- Added support for OneDir on Windows (#43)


v5.2.0 (2021-12-07)
===================

Fixed
-----

- Do not create files outside of /var/tmp/heist_<user>. This ensures the files /var/log/salt/minion and /etc/salt/minion.d do not get created. (#35)


Added
-----

- Add --onedir option to allow user to use onedir Salt packages. (#36)


v5.1.0 (2021-11-04)
===================

Fixed
-----

- Fix using heist-salt with Salt versions < 3003 when cleaning the connections. (#32)
- Set minion_type heist grain for bootstrap heist minions. (#33)
- Do not traceback when heist cannot connect to the target host. (#34)


Added
-----

- Add offline_mode option to skip downloading artifact step. (#31)


v5.0.0 (2021-10-25)
===================

Removed
-------

- Removed salt.artifacts.salt.fetch in favor of heist.artifacts.init.fetch
  Removed salt.artifacts.salt.verify_hash in favor of heist.artifacts.init.verify (#28)


Fixed
-----

- Allow heist to work with pkg version. For example 3004-1. It will also continue to allow use of old versioning (3004). (#30)


v4.0.0 (2021-08-18)
===================

Deprecated
----------

- Deprecate artifacts.salt.fetch and artifacts.salt.verify_hash in favor of heist.artifacts.init.{fetch,verify}. This will be removed in Heist-Salt version v5.0.0. (#28)


Fixed
-----

- Fix permissions for the deployed directory on the minion. (#29)


Added
-----

- Allow users to "bootstrap" their minions. This feature will allow a user to deploy a salt minion and point it to a different master. Heist will no longer manage this minion after deployment. (#5)
- Migrate artifact calls to heist/artifact/init.py (#27)


v3.0.0 (2021-07-15)
===================

Removed
-------

- Remove `accept_keys` in favore of `generate_keys`. (#23)


Fixed
-----

- Fixed setup.py to include the correct directory when building package. (#24)


Added
-----

- Add towncrier tool to the heist-salt project to help manage CHANGELOG.md file. (#22)
