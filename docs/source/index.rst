.. heist-salt documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to heist-salt's Documentation!
======================================


.. toctree::
   :maxdepth: 2
   :glob:

   topics/heist-salt
   topics/artifacts/*
   topics/bootstrap/*
   topics/transport/*
   tutorial/index
   releases/index
   topics/development/*
   topics/salt_minion.rst

Heist Salt Key
--------------
.. toctree::
   :maxdepth: 2
   :glob:

   topics/salt_key.rst

Heist Salt Artifacts
--------------------
.. toctree::
   :maxdepth: 2
   :glob:

   topics/artifacts/*

Bootstrap Heist Salt Minions
----------------------------
.. toctree::
   :maxdepth: 2
   :glob:

   topics/bootstrap/*

Get Involved
------------
.. toctree::
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license
   GitLab Repository <https://gitlab.com/saltstack/pop/heist-salt>

Indices and tables
==================

* :ref:`modindex`
