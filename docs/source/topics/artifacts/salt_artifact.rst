=============
Salt Artifact
=============

Heist-salt uses `Heist <https://gitlab.com/saltstack/pop/heist>`_ to deploy and
manage the Salt artifact. Heist-salt currently supports managing a single binary
or onedir package type, but support can be added in the future to support other
types of binaries. The single binary is built using `tiamat <https://gitlab.com/saltstack/pop/tiamat>`_.
Tiamat uses `pyinstaller <https://www.pyinstaller.org>`_ and can build either
a onedir or onefile build. The single binary in heist-salt, is a salt artifact built
with pyinstaller's onefile option. The onedir binary in heist-salt is a salt artifact
using pyinstaller's onedir option.

Heist automatically downloads artifacts from `repo.saltproject.io`_ and uses them to deploy
agents. In the case of `Salt`, these artifacts are either single binary or onedir versions of
`Salt` that have been built using the `salt-pkg`_ system.

Heist will automatically download the latest artifact from the repo, unless
it already exists in the `artifacts_dir` or a specific Salt version is set
via the `artifact_version` option. Heist will automatically detect the target
OS and download the appropriate binary. If `artifact_version` is set, heist
will download the `Salt` binary version from the repo if it exists.

If you want to deploy a custom version of Salt, or you want a salt binary
or onedir package that includes a different version of python, or more dependency libraries
this is very easy to do using the `salt-pkg`_ project. See the :ref:`custom_artifact`
docs for more information.

When the artifacts are downloaded from the remote repo they are placed in
your `artifacts_dir`. If you are running `heist` as root this dir can be
found at `/var/lib/heist/artifacts/<onedir/singlebin>`. If you are running as a user this dir
can be found at `~/.heist/artifacts/<onedir/singlebin>`.

The downloaded executables are in a tarball and are versioned with a version number following
the dash right after the name of the binary. It also includes the OS and architecture.
In the case of `salt` the file looks like this: `salt-3003-3-linux-amd64.tar.gz`. If you have
a custom build just name it something that has a higher version number, like `salt_3003-4-linux-amd64.tar.gz`.


OneDir vs Singlebin
===================

By default, Heist-Salt uses the singlebin version of the Salt package. If you need to use
the onedir option you will need to either pass `--onedir` to the cli command or set `onedir: True`
in your heist config file.

Singlebin
---------

The singlebin binary of Salt is built using Pyinstallers onefile option. It is a single
binary of Salt which encapsulates all dependencies and python to run salt. The artifact
is smaller in size than the onedir package, which leads to a quicker time to copy the
artifact to the target. Every time you run a Salt command with the single binary it needs
to extract all of the files to the temp directory. For Heist-Salt, this slows down the time it
takes to start up the Salt Minion. The singlebin cannot be daemonized, because when the fork
occurs it kills the process and pyinstaller deletes the temp directory. Although you cannot
run Salt with `-d` with the singlebin, you can use a service manager such as systemd to start
up the service. Heist-Salt uses systemd by default for singlebin artifacts. Please use the onedir
package if you need to daemonize the process.


Onedir
------

The onedir binary of Salt is built using Pyinstallers onefile option. It is a folder that
contains all of the depdencies and python to run salt. The artifact is larger in size compared
to the singlebin, which leads to a slower time to copy the artifact to the target. The onedir
already contains everything it needs in the directory so it does not need to extract everything
on each Salt command. This leads to a quicker time to start the Salt Minion compared to the singlebin.

.. list-table::
  :widths: 25 25 50
  :header-rows: 1

  * - Package Type
    - Salt-Minion Startup Time
    - Copy Artifact Time

  * - Singlebin
    - * Slow
    - * Fast

  * - Onedir
    - * Fast
    - * Slow

.. _`salt-pkg`: https://gitlab.com/saltstack/open/salt-pkg
.. _`repo.saltproject.io`: https://repo.saltproject.io/salt-singlebin/
