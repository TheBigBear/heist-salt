.. _custom_artifact:

Building Custom Salt Bins
=========================

To build a custom Salt binary, follow the instructions found in the
README.rst file in the salt-pkg repo:
https://gitlab.com/saltstack/open/salt-pkg/-/blob/master/README.md

The `salt-pkg`_ system uses `tiamat` and is completely self contained,
which makes it easy to customize your build of Salt. Just add deps to the
`pkgs/single_bin/sources/requirements.txt` file in the `salt-pkg`_ repo
and run, or add your additional requirements in the Salt repo's requirement
files.


salt_repo_url
-------------
Be default, heist-salt will query `repo.saltproject.io/salt/singlebin/repo.json <https://repo.saltproject.io/salt/singlebin/repo.json>`_ to return data about the artifacts. If you are using onedir packages, heist-salt will query
`repo.saltproject.io/salt/onedir/repo.json <https://repo.saltproject.io/salt/onedir/repo.json>`_ .
The returned json data will include the artifact name, version and hashes of the artifact.

You can set a custom repo by setting `salt_repo_url` to a url that points to your custom
repo. The custom repo needs to include a `repo.json` file and follow the directory structure of
`repo.saltproject.io/salt/<onedir/singlebin> <https://repo.saltproject.io/salt/>`_.


.. _`salt-pkg`: https://gitlab.com/saltstack/open/salt-pkg
