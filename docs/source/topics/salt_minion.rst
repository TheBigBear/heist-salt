Heist Minion
============

There are two ways to differentiate between a Heist-Salt minion
and a regular minion. A Heist-Salt minion communicates with the
master over SSH. Also, a Heist-Salt minion includes a `minion_type: heist`
grain. If you want to target only heist minions you can with
grains targeting on a Salt Master.


.. code-block:: bash

    salt -G 'minion_type:heist' test.version


Since Heist-Salt minions communicate over SSH if your Salt Master is attempting
to check for connected minions it will not work by default for Heist Minions. You
will need to set `detect_remote_minions` to be True in your Salt Master configuration.
This will check for connections the Master is connected to over port 22
by default. If you are running SSH on a different port you can change the
port with `remote_minions_port`. These settings will allow presence events, the
manage runner and any other features that detect connected minions in to a Salt Master
to work properly.
