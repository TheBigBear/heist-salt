Bootstrapping Minions to Existing Masters
=========================================

Heist can be used to bootstrap minions to connect to existing masters and not
be connected back through the ssh tunnel. This allows for minions to be deployed
and managed to many system in a very streamlined way.

The only change that needs to take place is that `bootstrap: True` and the master that
the target system needs to connect to is defined in minion_opts inside the roster:

.. code-block:: yaml

    hostname:
      username: frank
      password: horsebatterystaple
      bootstrap: True
      minion_opts:
        master: salt

This now tells the minion to deploy the salt minion artifact and connect to the specific
master located at the host named `salt`. When heist is stopped it will not delete anything
and will immediately kill the connection. With this approach you will need to manually
accept the key on the master that the minions are now communicating with.
