Generating the Salt Minion Key
==============================

By default, when a Salt heist minion is deployed the minion will
generate the key and copy back over to the master. On the master,
it is copied into the accepted keys folder in the master's `pki_dir`
directory. If you do not want to generate the key by default, you can
set the `generate_keys` config option to `False`. You will need to manually
accept the minion's key on the master if this is turned off.
