sphinx>=3.5.3
furo>=2021.3.20b30
sphinx-copybutton>=0.3.1
Sphinx-Substitution-Extensions>=2020.9.30.0
sphinx-notfound-page>=0.6
